﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void roomTemp_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double rt = Convert.ToDouble(roomTemp.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("Invalid room temperature input! Use only '0-9', '.'");
            }
        }

        private void dtTemp_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double dtt = Convert.ToDouble(dtTemp.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("Invalid drift tube temperature input! Use only '0-9', '.'");
            }
        }

        private void dtPres_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double dtp = Convert.ToDouble(dtPres.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("Invalid drift tube pressure input! Use only '0-9', '.'");
            }
        }

        private void msE_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double mse = Convert.ToDouble(msE.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("Invalid MS Energy input! Use only '0-9', '.'");
            }
        }

        private void fromE_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int frome = Convert.ToInt32(fromE.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("Invalid E/N Range input! Use only '0-9'");
            }
        }

        private void toE_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int toe = Convert.ToInt32(toE.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("Invalid E/N Range input! Use only '0-9'");
            }
        }

        private void stepE_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int stepe = Convert.ToInt32(stepE.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("Invalid E/N Range input! Use only '0-9'");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Created by: Kos Breiev.\nIonicon Analytik GmbH.\nwww.ionicon.com", "About");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double Avag = 6.02 * 10E23;
            double stVol = 22414;
            double stTemp = 273.15;
            double stPres = 1013.25;
            double numbDens = (Avag * stTemp * Double.Parse(dtPres.Text)) / (stVol * Math.Sqrt((Double.Parse(dtTemp.Text) + 273.15) * (Double.Parse(roomTemp.Text) + 273.15)) * stPres);
            double EN = Double.Parse(fromE.Text);
            double ptrL;
            double colE;
            StringBuilder str = new StringBuilder();
            while (EN <= Double.Parse(toE.Text))
            {
                ptrL = (EN * numbDens * 1.1 / 10E17 + Double.Parse(msE.Text));
                colE = (EN * numbDens * 9.1 / 10E17 + ptrL);
                str.Append("\nE/N, Td:   ").Append((int)EN).Append("\tCollision Energy:   ").Append((double)Math.Round(colE * 10) / 10).Append("\tPTR Lens:   ").Append((double)Math.Round(ptrL * 10) / 10);
                EN += Double.Parse(stepE.Text);
            }
            MessageBox.Show(str.ToString(), "E/N Calculator");
        }
    }
}