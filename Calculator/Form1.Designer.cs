﻿namespace Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.roomTemp = new System.Windows.Forms.TextBox();
            this.dtTemp = new System.Windows.Forms.TextBox();
            this.dtPres = new System.Windows.Forms.TextBox();
            this.msE = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fromE = new System.Windows.Forms.TextBox();
            this.toE = new System.Windows.Forms.TextBox();
            this.stepE = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // roomTemp
            // 
            this.roomTemp.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roomTemp.Location = new System.Drawing.Point(35, 34);
            this.roomTemp.Name = "roomTemp";
            this.roomTemp.Size = new System.Drawing.Size(100, 25);
            this.roomTemp.TabIndex = 0;
            this.roomTemp.Text = "25";
            this.roomTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.roomTemp.TextChanged += new System.EventHandler(this.roomTemp_TextChanged);
            // 
            // dtTemp
            // 
            this.dtTemp.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtTemp.Location = new System.Drawing.Point(35, 100);
            this.dtTemp.Name = "dtTemp";
            this.dtTemp.Size = new System.Drawing.Size(100, 25);
            this.dtTemp.TabIndex = 1;
            this.dtTemp.Text = "100";
            this.dtTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dtTemp.TextChanged += new System.EventHandler(this.dtTemp_TextChanged);
            // 
            // dtPres
            // 
            this.dtPres.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.dtPres.Location = new System.Drawing.Point(35, 164);
            this.dtPres.Name = "dtPres";
            this.dtPres.Size = new System.Drawing.Size(100, 25);
            this.dtPres.TabIndex = 2;
            this.dtPres.Text = "0.8";
            this.dtPres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dtPres.TextChanged += new System.EventHandler(this.dtPres_TextChanged);
            // 
            // msE
            // 
            this.msE.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.msE.Location = new System.Drawing.Point(35, 259);
            this.msE.Name = "msE";
            this.msE.Size = new System.Drawing.Size(100, 25);
            this.msE.TabIndex = 3;
            this.msE.Text = "9.7";
            this.msE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.msE.TextChanged += new System.EventHandler(this.msE_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 19);
            this.label1.TabIndex = 5;
            this.label1.Text = "Room Temperature, °C";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 19);
            this.label2.TabIndex = 6;
            this.label2.Text = "Drift Tube Temperature, °C";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 19);
            this.label3.TabIndex = 7;
            this.label3.Text = "Drift Tube Pressure, mbar";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label4.Location = new System.Drawing.Point(31, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 19);
            this.label4.TabIndex = 8;
            this.label4.Text = "MS Energy, V";
            // 
            // fromE
            // 
            this.fromE.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.fromE.Location = new System.Drawing.Point(151, 223);
            this.fromE.Name = "fromE";
            this.fromE.Size = new System.Drawing.Size(100, 25);
            this.fromE.TabIndex = 10;
            this.fromE.Text = "100";
            this.fromE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.fromE.TextChanged += new System.EventHandler(this.fromE_TextChanged);
            // 
            // toE
            // 
            this.toE.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.toE.Location = new System.Drawing.Point(316, 223);
            this.toE.Name = "toE";
            this.toE.Size = new System.Drawing.Size(100, 25);
            this.toE.TabIndex = 11;
            this.toE.Text = "200";
            this.toE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toE.TextChanged += new System.EventHandler(this.toE_TextChanged);
            // 
            // stepE
            // 
            this.stepE.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.stepE.Location = new System.Drawing.Point(232, 302);
            this.stepE.Name = "stepE";
            this.stepE.Size = new System.Drawing.Size(100, 25);
            this.stepE.TabIndex = 12;
            this.stepE.Text = "10";
            this.stepE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.stepE.TextChanged += new System.EventHandler(this.stepE_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label6.Location = new System.Drawing.Point(175, 189);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 19);
            this.label6.TabIndex = 13;
            this.label6.Text = "From, Td";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label7.Location = new System.Drawing.Point(342, 189);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 19);
            this.label7.TabIndex = 14;
            this.label7.Text = "To, Td";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(253, 268);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 19);
            this.label8.TabIndex = 15;
            this.label8.Text = "Step, Td";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(211, 12);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(151, 149);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.button1.Location = new System.Drawing.Point(381, 317);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Calculate!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.button2.Location = new System.Drawing.Point(381, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "About";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label5.Location = new System.Drawing.Point(266, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 19);
            this.label5.TabIndex = 19;
            this.label5.Text = "E/N";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(464, 350);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.stepE);
            this.Controls.Add(this.toE);
            this.Controls.Add(this.fromE);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.msE);
            this.Controls.Add(this.dtPres);
            this.Controls.Add(this.dtTemp);
            this.Controls.Add(this.roomTemp);
            this.Name = "Form1";
            this.Text = "E/N Calculator";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox roomTemp;
        private System.Windows.Forms.TextBox dtTemp;
        private System.Windows.Forms.TextBox dtPres;
        private System.Windows.Forms.TextBox msE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox fromE;
        private System.Windows.Forms.TextBox toE;
        private System.Windows.Forms.TextBox stepE;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label5;
    }
}

